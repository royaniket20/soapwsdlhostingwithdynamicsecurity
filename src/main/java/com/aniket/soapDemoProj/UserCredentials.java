package com.aniket.soapDemoProj;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserCredentials {
    @NotNull(message = "userid is mandatory")
    private String userid;
    @NotNull(message = "password is mandatory")
    private String pass;
    private String id;
}
