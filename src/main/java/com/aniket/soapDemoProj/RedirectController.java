package com.aniket.soapDemoProj;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class RedirectController {

    @RequestMapping("/")
    public String index() {
        return "redirect:swagger-ui/index.html";
    }
}
