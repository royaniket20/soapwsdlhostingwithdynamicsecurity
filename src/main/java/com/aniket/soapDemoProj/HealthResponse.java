package com.aniket.soapDemoProj;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class HealthResponse {
    private Date lastUpdated;
    private String status;
}
