package com.aniket.soapDemoProj;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.restart.RestartEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SpringBootApplication
@Configuration
@EnableWebSecurity
@Slf4j
@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class})
public class SoapDemoProjApplication {

	public static final Map<String , UserDetails> users = new ConcurrentHashMap<>();
	public static void main(String[] args) {
		SpringApplication.run(SoapDemoProjApplication.class, args);
	}

	@Bean
	public UserDetailsService userDetailsService()
	{
		List<UserDetails> userList = new ArrayList<>();
		userList.add( User.withUsername("ADMIN").password("{noop}"+"ADMIN").authorities("ADMIN").build());
		users.entrySet().stream().forEach(item->{
			userList.add( User.withUsername(item.getValue().getUsername()).password("{noop}"+item.getValue().getPassword()).authorities("ADMIN").build());
		});
		return new InMemoryUserDetailsManager(userList);
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		 httpSecurity.csrf().disable();

		httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/api/**").permitAll());
		httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/","/v3/api-docs/**","/swagger-ui/**" ,"/v3/api-docs").permitAll());
		httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/dummySoap/open/**").permitAll());
		httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/dummySoap/secure/**").authenticated());
		httpSecurity.httpBasic();
		return httpSecurity.build();
	}

	@Bean
	public RestartEndpoint restartEndpoint(){
		return new RestartEndpoint();
	}

}
