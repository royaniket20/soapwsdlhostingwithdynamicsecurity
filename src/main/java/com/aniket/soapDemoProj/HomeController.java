package com.aniket.soapDemoProj;

import com.google.gson.Gson;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.restart.RestartEndpoint;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.aniket.soapDemoProj.SoapDemoProjApplication.users;

@RestController
@RequestMapping("/api")
public class HomeController {

    @Autowired
    private RestartEndpoint restartEndpoint;
    @GetMapping("/health")
    public HealthResponse getHealth()
    {
       return HealthResponse.builder().
               status("System is Up!!!").
               lastUpdated(new Date()).
               build();
    }

    @PostMapping ("/user")
    public HealthResponse addUser(@RequestBody @Valid UserCredentials userCredentials)
    {
        Optional<Map.Entry<String, UserDetails>> isAvailable = users.entrySet().stream().filter(item ->
                item.getValue().getUsername().equals(userCredentials.getUserid())
        ).findFirst();
        if(isAvailable.isPresent()){
            return HealthResponse.builder().
                    status("User already present inside the System - "+new Gson().toJson(userCredentials)).
                    lastUpdated(new Date()).
                    build();
        }else{
            new Thread(()->{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                restartEndpoint.restart();
            }).start();
            String id = UUID.randomUUID().toString();
            UserDetails user = User.builder().username(userCredentials.getUserid()).password(userCredentials.getPass()).authorities("ADMIN").build();
            users.put(id,user);
            userCredentials.setId(id);
            return HealthResponse.builder().
                    status("User added to the System - "+new Gson().toJson(userCredentials)).
                    lastUpdated(new Date()).
                    build();
        }

    }


    @PutMapping  ("/user")
    public HealthResponse  updateUser(@RequestBody @Valid UserCredentials userCredentials)
    {
        Optional<Map.Entry<String, UserDetails>> isAvailable = users.entrySet().stream().filter(item ->
                item.getKey().equals(userCredentials.getId())
        ).findFirst();
        if(!isAvailable.isPresent()){
            return HealthResponse.builder().
                    status("User ID Not supplied for update - "+new Gson().toJson(userCredentials)).
                    lastUpdated(new Date()).
                    build();
        }else{
            new Thread(()->{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                restartEndpoint.restart();
            }).start();
            String id = userCredentials.getId();
            UserDetails user = User.builder().username(userCredentials.getUserid()).password(userCredentials.getPass()).authorities("ADMIN").build();
            users.put(id,user);
            return HealthResponse.builder().
                    status("User updated to the System - "+new Gson().toJson(userCredentials)).
                    lastUpdated(new Date()).
                    build();
        }

    }


    @GetMapping   ("/user")
    public List<UserCredentials>  getUsers()
    {
        if(users.isEmpty()){
            return new ArrayList<>();
        }else{
           return  users.entrySet().stream().map(item->
                 UserCredentials.builder()
                        .id(item.getKey())
                        .userid(item.getValue().getUsername())
                        .pass(item.getValue().getPassword())
                        .build()).collect(Collectors.toList());
        }
    }


    @RequestMapping(value = "/wsdl/v1/RMA", method = RequestMethod.GET, produces = { "application/xml", "text/xml" } )
    public String getV1SwdlRMA(){
        try {
            ClassPathResource resource = new ClassPathResource("mockRmaDynamicWebPortBinding.xml");
            byte[] binaryData = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return  new String(binaryData, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/wsdl/v1/WFS", method = RequestMethod.GET, produces = { "application/xml", "text/xml" } )
    public String getV1SwdlWFS(){
        try {
            ClassPathResource resource = new ClassPathResource("mockWFSOriginationsServiceBinding.xml");
            byte[] binaryData = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return  new String(binaryData, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/wsdl/v2/COUNTRY", method = RequestMethod.GET, produces = { "application/xml", "text/xml" } )
    public String getV2SwdlCountry(){
        try {
            ClassPathResource resource = new ClassPathResource("CountryInfoService.xml");
            byte[] binaryData = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return  new String(binaryData, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    @RequestMapping(value = "/wsdl/v2/NUMBER", method = RequestMethod.GET, produces = { "application/xml", "text/xml" } )
    public String getV2SwdlNumber(){
        try {
            ClassPathResource resource = new ClassPathResource("NumberConversion.wsdl");
            byte[] binaryData = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return  new String(binaryData, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }




}
